import { Controller, Get, Post, Body, Patch, Param, Delete, Res } from '@nestjs/common';
import { PdfService } from './pdf.service';
import { CreatePdfDto } from './dto/create-pdf.dto';
import { UpdatePdfDto } from './dto/update-pdf.dto';
import { Response } from 'express';

@Controller('pdf')
export class PdfController {
  constructor(private readonly pdfService: PdfService) { }

  @Get('generate')
  async generateAndReturnPdf(@Res() res: Response): Promise<void> {
    const pdfPath = await this.pdfService.genreatePdf()
    res.sendFile(pdfPath);
  }

  @Get('createpdf')
  async getPdf(@Res() res: Response) {
    const data = {
      createdDate: '2024-04-23T15:31:07.000Z',
      cId: 'b32d5d05-916c-4e7b-92ef-6f89b9aeb38b',
      cCode: 'T-00060',
      cCategory: 'TRAVEL',
      cName_th: 'มหาปัญญาวิทยาลัย',
      cName_en: 'Mahapanya Vidayalai College',
      cName_cn: 'Mahapanya Vidayalai College',
      cDesc_th: 'มหาปัญญาวิทยาลัย เป็นสถานที่น่าเที่ยวอีกแห่งหนึ่งของหาดใหญ่...',
      cDesc_en: 'Mahapanya Vidayalai School It is another interesting place...',
      cDesc_cn: 'Mahapanya Vidayalai School It is another interesting place...',
      cLocation_th: 'มหาปัญญาวิทยาลัย 635/1  ถนนธรรมนูญวิถี อำเภอหาดใหญ่ สงขลา 90110',
      tel: '074243558',
      // ข้อมูลอื่น ๆ
    };

    const pdfBuffer = await this.pdfService.createPdf(data);
    res.set({
      'Content-Type': 'application/pdf',
      'Content-Disposition': 'attachment; filename=output.pdf',
      'Content-Length': pdfBuffer.length,
    });
    res.end(pdfBuffer);
  }

  @Post()
  create(@Body() createPdfDto: CreatePdfDto) {
    return this.pdfService.create(createPdfDto);
  }

  @Get()
  findAll() {
    return this.pdfService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.pdfService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePdfDto: UpdatePdfDto) {
    return this.pdfService.update(+id, updatePdfDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.pdfService.remove(+id);
  }
}
